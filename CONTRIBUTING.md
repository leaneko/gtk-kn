# Contributing to gtk-kn

Thank you for considering contributing to our project! We welcome any contributions that help make our project better.
Whether it is fixing a bug, adding a new feature, or improving documentation, your help is appreciated.

Before you start, please read this guide on how to contribute to our project.

## Issues

If you find a bug or have a feature request, please submit an issue on
our [issue tracker](https://gitlab.com/gtk-kn/gtk-kn/-/issues). Before submitting an issue, please make sure that
it has not already been reported by searching the existing issues. When submitting an issue, please provide as much
information as possible, such as:

- A clear and concise title that summarizes the issue.
- A detailed description of the issue, including steps to reproduce it.
- The version of the project you are using.
- Any relevant logs or error messages.
- Any screenshots or videos that help illustrate the issue.

## Merge Requests

If you want to contribute code, please follow these steps:

1. Create a new issue for the feature request or bug fix you want to work on. This will allow the maintainers to review
   and discuss the idea with you before you invest a lot of time and effort.
2. Fork the repository and create a new branch for your changes.
3. Write code that follows our coding standards, is well-documented, and includes tests where appropriate.
4. Make sure the code has been tested thoroughly and works as expected.
5. Submit a merge request on [GitLab](https://gitlab.com/gtk-kn/gtk-kn/-/merge_requests), including a clear and concise
   title that summarizes the changes made, a detailed description of the changes, and any relevant issues that are being
   addressed.
6. Be patient while the maintainers review your changes. We will do our best to review and merge them as quickly as
   possible.

When submitting a merge request, please provide as much information as possible, such as:

- A clear and concise title that summarizes the merge request.
- A detailed description of the changes made.
- Any relevant issues that are being addressed by the merge request.
- Screenshots or videos that help illustrate the changes, if applicable.

We appreciate all contributions and will do our best to review and merge them as quickly as possible. Thank you for your
help in making our project better!
